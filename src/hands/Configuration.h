#pragma once

#include "Value.h"

#include <map>
#include <string>

class Configuration
{
  public:
    Configuration(const std::string &file, const std::string &default = "");

    void setValue(const std::string &section, const std::string &key, const Value &value);

    Value getValue(const std::string &section, const std::string &key);

  private:
    std::string m_file;
    std::map<std::string, std::map<std::string, Value>> m_settings; // section -> parameter -> value

    void readFile();
    void saveFile();
};
