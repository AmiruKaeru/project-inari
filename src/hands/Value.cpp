#include "Value.h"

#include <cmath>

Value::Value()
{
}

Value::Value(const std::string &value)
    : m_value(value)
{
}

Value::Value(bool value)
    : m_value(std::to_string(value))
{
}

Value::Value(int value)
    : m_value(std::to_string(value))
{
}

Value::Value(float value)
    : m_value(std::to_string(value))
{
}

std::string Value::toString() const
{
    return m_value;
}

bool Value::toBool()
{
    return toInt() <= 0 ? false : true;
}

int Value::toInt()
{
    return static_cast<int>(std::round(toFloat()));
}

float Value::toFloat()
{
    return isNumber() ? std::stof(m_value) : 0.f;
}

bool Value::isNumber()
{
    return !m_value.empty() && std::find_if(m_value.begin(), m_value.end(), [](char c) {
                                   return !std::isdigit(c);
                               }) == m_value.end();
}