#pragma once

#include <GL/gl3w.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <string>

class Shader
{
  public:
    Shader();
    Shader(const std::string &vertex_path, const std::string &fragment_path);

    void compile(const std::string &vertex_path, const std::string &fragment_path);
    void use() const;

    Shader &setFloat(const std::string &name, GLfloat value, GLboolean use_shader = false);
    Shader &setInteger(const std::string &name, GLint value, GLboolean use_shader = false);
    Shader &setVector2f(const std::string &name, GLfloat x, GLfloat y, GLboolean use_shader = false);
    Shader &setVector2f(const std::string &name, const glm::vec2 &value, GLboolean use_shader = false);
    Shader &setVector3f(const std::string &name, GLfloat x, GLfloat y, GLfloat z, GLboolean use_shader = false);
    Shader &setVector3f(const std::string &name, const glm::vec3 &value, GLboolean use_shader = false);
    Shader &setVector4f(const std::string &name, GLfloat x, GLfloat y, GLfloat z, GLfloat w,
                        GLboolean use_shader = false);
    Shader &setVector4f(const std::string &name, const glm::vec4 &value, GLboolean use_shader = false);
    Shader &setMatrix4(const std::string &name, const glm::mat4 &value, GLboolean use_shader = false);

    GLuint getId();

  private:
    enum CompileType
    {
        VERTEX = 0,
        FRAGMENT,
        PROGRAM
    };

    bool checkCompileError(const GLuint &shader, const CompileType &type);
    bool readFileToString(const std::string &path, std::string &output);

    GLuint m_id;
};
